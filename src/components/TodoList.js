import React, { useState, useEffect } from 'react'
import TodoItem from './TodoItem'
import Button from './Button'

const TodoList = () => {
    const [todos, setTodos] = useState([])

    const [sort, setSort] = useState(false)
    const [filter, setFilter] = useState(false)

    const [input, setInput] = useState('')

    const handleInputChange = event => {
        const newValue = event.target.value
        if(newValue.length > 35) {
            console.warn('String too long')
        } else {
            setInput(event.target.value)
        }
    }

    // run once when app start
    useEffect(()=> {
        const localTodos = getTodosLocalStorage()
        if(localTodos.length > 0)
            setTodos(localTodos)
    }, [])

    // run whenever todos changes
    useEffect(() => {
        // save to localStorage with a stringified todos array
        const todosString = JSON.stringify(todos)
        localStorage.setItem('todos', todosString)
    }, [todos])

    // get localStorage and transform createdAt property from string to Date object
    const getTodosLocalStorage = () => {
        const todosString = localStorage.getItem('todos')
        let tempTodos = []

        if(todosString) {
            tempTodos = JSON.parse(todosString)
            console.log(tempTodos, 'parsed')
            tempTodos = tempTodos.map(todo => ({
                ...todo,
                createdAt: new Date(todo.createdAt)
            }))
        }

        return tempTodos
    }

    const handleAddTodo = () => {
        const newTodoItem = {
            id: Date.now() + Math.floor(Math.random() * 100),
            text: input,
            completed: false,
            createdAt: new Date()
        }
        setTodos([newTodoItem, ...todos])
    }

    const handleClearInput = () => setInput('')

    const handleToggleTodo = id => {
        // console.log(id)
        const newTodos = todos.map(todo => {
            if(todo.id === id) {
                return {
                    ...todo,
                    completed: !todo.completed
                }
            }
            return todo
        })
        setTodos(newTodos)
    }

    const handleDeleteTodo = id => {
        const newTodos = todos.filter(todo => todo.id !== id)

        setTodos(newTodos)
    }

    const handleEditTodo = (id, text) => {
        const newTodos = todos.map(todo => {
            if(todo.id === id) {
                return {
                    ...todo,
                    text
                }
            }
            return todo
        })
        setTodos(newTodos)
    }

    const renderTodoItems = () => {
        let todosToRender = []

        if(sort && filter) {
            todosToRender = todos.filter(todo => !todo.completed)
            todosToRender = todosToRender.sort((a,b) => a.createdAt - b.createdAt)
        } else if (!sort && filter) {
            todosToRender = todos.filter(todo => !todo.completed)
            todosToRender = todosToRender.sort((a,b) => b.createdAt - a.createdAt)
        } else if (sort && !filter) {
            todosToRender = todos.sort((a,b) => a.createdAt - b.createdAt)
        } else if (!sort && !filter) {
            todosToRender = todos.sort((a,b) => b.createdAt - a.createdAt)
        }

        return todosToRender.map(
            todo => (
                <TodoItem
                    key={todo.id}
                    todo={todo}
                    handleToggleTodo={handleToggleTodo}
                    handleDeleteTodo={handleDeleteTodo}
                    handleEditTodo={handleEditTodo}
                />
            )
        )
    }

    return (
        <div className="w-50 p-3 m-3 mx-auto card">
            <h1 className="blooda">TodoList</h1>
            <div>
                <button 
                    style={{ background: sort ? 'green' : 'white'}} 
                    onClick={() => setSort(!sort)}
                >
                    sort
                </button>
                <button 
                    style={{ background: filter ? 'green' : 'white'}} 
                    onClick={() => setFilter(!filter)}
                >
                        filter
                </button>
            </div>
            
            <div className="input-group mb-3">
                <input className="form-control" value={input} onChange={handleInputChange}/>
                <div className="input-group-append">
                    <Button text="x" onClick={handleClearInput}/>
                    <Button text="Add" onClick={handleAddTodo} />
                </div>
            </div>
            
            <ul className="container todo-list" >
                {renderTodoItems()}
            </ul>
        </div>
    )
}

export default TodoList