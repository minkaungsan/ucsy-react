import React, { useState, useEffect } from 'react'
import Heading from './Heading'
import Button from './Button'

const Counter = () => {
    const [count, setCount] = useState(Number(localStorage.getItem('count') || 0));

    // useEffect(() => {
    //   const localCount = localStorage.getItem('count')
    //   if(localCount)
    //     setCount(Number(localCount))
    // }, [])

    useEffect(()=>{
      // do something whenever count state change
      document.title = `Counter ${count}`
      localStorage.setItem('count', count)
    }, [count])

    return (
      <div>
        <Heading>Counter</Heading>
        <h2>{count}</h2>
        <Button text="-" onClick={() => setCount(count - 1)} />
        <Button text="+" onClick={() => setCount(count + 1)} />
      </div>
    )
  }

export default Counter