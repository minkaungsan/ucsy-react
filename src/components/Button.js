import React from 'react'

const Button = ({ onClick, text }) => (
    <button onClick={onClick} className="form-control">{text}</button>
  )

export default Button