import React, {useState} from 'react'
import Button from './Button'

const TodoItem = ({ todo, handleToggleTodo, handleDeleteTodo, handleEditTodo }) => {
    const [isEditing, setIsEditing] = useState(false)
    const [editText, setEditText] = useState(todo.text)

    const acceptEdit = (id, text) => {
        handleEditTodo(id, text)
        setIsEditing(false)
    }

    const cancelEdit = () => {
        setEditText(todo.text)
        setIsEditing(false)
    }

    return (
        <li className="row">
            <input
                className="col-md-2"
                type="checkbox"
                checked={todo.completed}
                onChange={() => handleToggleTodo(todo.id)}
            />
            {isEditing ? 
            <>
                <div className="col-md-6 blooda">
                    <input 
                        className="edit-input" 
                        value={editText} 
                        onChange={event => setEditText(event.target.value)} 
                    />
                </div>
                <div className="col-md-2">
                    <Button text="Ok" onClick={() => acceptEdit(todo.id, editText)}/>
                </div>
                <div className="col-md-2">
                    <Button text="Cancel" onClick={cancelEdit}/>
                </div>
            </>           
            : 
            <>
                <div
                    className="col-md-6 blooda"
                    style={{
                        textDecoration: todo.completed ? 'line-through' : 'none'
                    }}
                >
                    {todo.text}
                </div>
                <div className="col-md-2">
                    <Button text="Edit" onClick={() => setIsEditing(true)} />
                </div>
                <div className="col-md-2">
                    <Button text="Delete" onClick={() => handleDeleteTodo(todo.id)} />
                </div>
            </>
            }
        </li>
    )
}

export default TodoItem