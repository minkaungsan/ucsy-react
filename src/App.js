import React from 'react';
import { Link, Route, Switch, useParams, Redirect, useLocation } from 'react-router-dom'
import Counter from './components/Counter';
import TodoList from './components/TodoList'

const Home = () => (
  <div>
    <h2>Welcome to home page.</h2>
  </div>
)

const Profile = () => {
  const { id } = useParams()
  const searchString = useLocation().search
  const query = new URLSearchParams(searchString)


  return Number(id) !== 20 ? (
    <div>
      <div>User Id is :{id}</div>
      <div>Name is : {query.get('name')}</div>
      <div>Age is : {query.get('age')}</div>
      <div>Gender is : {query.get('gender')}</div>
    </div>
  ) : 
  <Redirect 
    to={{
      pathname:"/"
    }}
  />
}

const NoMatch = () => <h1>Error 404: no page found!</h1>

const App = () => {
  return (
    <div>
      <nav>
        <Link to="/">Home</Link>
        <Link to="/counter">Counter</Link>
        <Link to="/todo">Todo</Link>
        <Link to={`/profile/23?name=Max`}>Profile</Link>
      </nav>
      <hr/>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/counter">
          <Counter />
        </Route>
        <Route path="/todo">
          <TodoList />
        </Route>
        <Route path="/profile/:id">
          <Profile />
        </Route>
        <Route path="*">
          <NoMatch/>
        </Route>
      </Switch>
    </div>
  )
}

export default App;
